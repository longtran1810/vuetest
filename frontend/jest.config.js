module.exports = {
  preset: '@vue/cli-plugin-unit-jest',
  globals: {
    __USE_BUILD__: process.argv.indexOf ('-use-build') >= 0,
    __BROWSER__: true,
  },
  transform: {
    '^.+\\.vue$': 'vue-jest',
    '^.+\\js$': 'babel-jest'
  },
  moduleFileExtensions: ['vue', 'js', 'json', 'jsx', 'ts', 'tsx', 'node'],
  setupFiles: ['./setup.js'],
};
