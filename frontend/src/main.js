import { createApp } from "vue";
import { router } from "./router";
import Layout from "./layout/Layout.vue";

require("@/assets/sass/main.scss");

createApp(Layout)
  .use(router)
  .mount("#app");
