export default async function (url, options) {
  try {
    const res = await fetch (url, options);
    const json = await res.json ();
    return json;
  } catch (errors) {
    return;
  }
}
