import {mount} from '@vue/test-utils';
import Table from '../../src/components/Table.vue';

describe ('props', () => {
  it ('pass props to table components', () => {
    const wrapper = mount (Table, {
      props: {tableContents: [{name: '1', code: 'x1'}]},
    });
    expect (wrapper.props ()).toEqual ({
      tableContents: [{name: '1', code: 'x1'}],
    });
  });
  it ('does not pass props to table components', () => {
    const wrapper = mount (Table, {});
    expect (wrapper.props ('tableContents')).toEqual ([]);
  });
  it ('render table components', () => {
    const tableContents = [{name: '1', code: 'x1'}];
    const wrapper = mount (Table, {
      props: {tableContents},
    });
    expect (wrapper.findAll ('tbody>tr')).toHaveLength (tableContents.length);
  });
  it ('snapshot', () => {
    const tableContents = [{name: '1', code: 'x1'}];

    const wrapper = mount (Table, {
      propsData: {tableContents},
    });
    expect (wrapper.html ()).toMatchSnapshot ();
  });
});
