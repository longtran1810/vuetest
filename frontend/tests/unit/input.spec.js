import { mount } from "@vue/test-utils";
import SearchInput from "../../src/components/SearchInput.vue";

describe("setValue", () => {
  describe("on input change", () => {
    it("on change input value", async () => {
      const wrapper = mount(SearchInput);
      const input = wrapper.find('input[type="text"]');
      await input.setValue("helloworld");
      expect(input.element.value).toBe("helloworld");
    });
  });
});
